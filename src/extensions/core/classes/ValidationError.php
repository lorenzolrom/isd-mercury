<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * DESCRIPTION
 *
 * User: lromero
 * Date: 2/07/2019
 * Time: 10:49 PM
 */


namespace core\classes;


class ValidationError extends \SplEnum
{
    const VALUE_IS_OK = 0;
    const VALUE_IS_NULL = 1;
    const VALUE_IS_TOO_SHORT = 2;
    const VALUE_IS_TOO_LONG = 3;
    const VALUE_IS_ALREADY_TAKEN = 4;
    const VALUE_IS_UNCHANGED = 5;
    const VALUE_IS_INVALID = 6;
}