<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Database Exception
 *
 * User: lromero
 * Date: 2/06/2019
 * Time: 8:19 PM
 */


namespace core\classes;


use Throwable;

class DatabaseException extends \Exception
{
    private $sqlCode;

    /**
     * DatabaseException constructor.
     * @param string $message
     * @param int $code
     * @param int $sqlCode
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, $sqlCode = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->sqlCode = $sqlCode;
    }

    /**
     * @return int SQL error code
     */
    public function getSQLCode()
    {
        return $this->sqlCode;
    }
}