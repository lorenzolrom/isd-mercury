<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Database Connection Object
 *
 * User: lromero
 * Date: 2/06/2019
 * Time: 8:16 PM
 */


namespace core\classes;


class DatabaseConnection
{
    private $handler; // Database interaction object

    /**
     * DatabaseConnection constructor.
     * @throws DatabaseException In event of database connection failure
     */
    public function __construct()
    {
        try
        {
            $this->handler = new \PDO("mysql:host=" . MERCURY_DATA_HOST . ";dbname=" . MERCURY_DATA_DATABASE,
                MERCURY_DATA_USER,
                MERCURY_DATA_PASSWORD,
                array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
        }
        catch(\PDOException $e)
        {
            throw new DatabaseException("Failed To Establish Connection To The Database", 1001, $e->getCode());
        }
    }

    /**
     * @param string $query Raw SQL query string
     * @return mixed Results of PHP query
     * @throws DatabaseException In event of query failure
     */
    public function query($query)
    {
        try
        {
            return $this->handler->query($query);
        }
        catch(\PDOException $e)
        {
            throw new DatabaseException("Direct Query Failure", 1002, $e->getCode());
        }
    }

    /**
     * Returns a prepared SQL statement for this connection handler
     * @param string $query SQL query string
     * @return PreparedStatement
     */
    public function prepare($query)
    {
        return new PreparedStatement($this->handler, $query);
    }

    /**
     * @return string Row ID of the last inserted record
     */
    public function getLastInsertId()
    {
        return $this->handler->lastInsertId();
    }

    /**
     * Starts a database transaction and sets auto-commit mode to FALSE
     * @return bool Transaction Succeeded
     * @throws DatabaseException In event transaction cannot be started
     */
    public function startTransaction()
    {
        try
        {
            $this->handler->beginTransaction();
            return TRUE;
        }
        catch(\PDOException $e)
        {
            throw new DatabaseException("Failed To Begin Transaction", 1003, $e->getCode());
        }
    }

    /**
     * Rollback current database transaction and sets auto-commit mode to TRUE
     * @return bool Rollback succeeded
     * @throws DatabaseException In event transaction cannot be rolled back
     */
    public function rollback()
    {
        try
        {
            $this->handler->rollBack();
            return TRUE;
        }
        catch(\PDOException $e)
        {
            throw new DatabaseException("Failed To Rollback Transaction", 1004, $e->getCode());
        }
    }

    /**
     * Commits current database transaction and sets auto-commit mode to TRUE
     * @return bool Commit succeeded
     * @throws DatabaseException In event transaction cannot be committed
     */
    public function commit()
    {
        try
        {
            $this->handler->commit();
            return TRUE;
        }
        catch(\PDOException $e)
        {
            throw new DatabaseException("Failed To Commit Transaction", 1005, $e->getCode());
        }
    }

    /**
     * Un-sets the database connection, symbolically closing the connection
     */
    public function close()
    {
        $this->handler = NULL;
    }
}