<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * DESCRIPTION
 *
 * User: lromero
 * Date: 2/06/2019
 * Time: 8:36 PM
 */


namespace core\classes;


use Throwable;

class FatalException extends \Exception
{
    public function __construct($message = "", $code = 1, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}