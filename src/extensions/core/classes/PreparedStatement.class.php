<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Prepared Database Statement
 *
 * User: lromero
 * Date: 2/06/2019
 * Time: 8:27 PM
 */


namespace core\classes;


class PreparedStatement
{
    private $statement; // Stored SQL query statement

    /**
     * PreparedStatement constructor.
     * @param \PDO $handler A DatabaseConnection object
     * @param string $query SQL query string
     */
    public function __construct($handler, $query)
    {
        $this->statement = $handler->prepare($query);
    }

    /**
     * @param $index mixed Either numeric indicator or string substitution
     * @param $parameter mixed Parameter to bind
     */
    public function bindParam($index, &$parameter)
    {
        $this->statement->bindParam($index, $parameter);
    }

    /**
     * @param mixed $index Either numeric indicator or string substitution
     * @param mixed $value Value to be bound
     */
    public function bindValue($index, $value)
    {
        $this->statement->bindValue($index, $value);
    }

    /**
     * @return bool Execution succeeded
     * @throws DatabaseException If execution fails
     */
    public function execute()
    {
        try
        {
            $this->statement->execute();
            return TRUE;
        }
        catch(\PDOException $e)
        {
            throw new DatabaseException("Prepared Statement Execution Failed", 1006, $e->getCode());
        }
    }

    /**
     * @return mixed Next row in results
     */
    public function fetch()
    {
        return $this->statement->fetch();
    }

    /**
     * @param bool $fetchType Option for how data should be returned
     * @param int $fetchArgument Arguments for fetchType
     * @return mixed Array of row results
     */
    public function fetchAll($fetchType = FALSE, $fetchArgument = 0)
    {
        if($fetchType !== FALSE)
            return $this->statement->fetchAll($fetchType, $fetchArgument);
        else
            return $this->statement->fetchAll();
    }

    /**
     * @return mixed Next column in results
     */
    public function fetchColumn()
    {
        return $this->statement->fetchColumn();
    }

    /**
     * @return mixed Count of returned rows
     */
    public function getRowCount()
    {
        return $this->statement->rowCount();
    }
}