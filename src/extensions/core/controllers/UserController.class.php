<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * DESCRIPTION
 *
 * User: lromero
 * Date: 2/07/2019
 * Time: 10:13 PM
 */


namespace core\controllers;

use core\factories\UserFactory;
use core\models\User;

class UserController
{
    /**
     * @param int $id Numerical I.D. of the user
     * @return User
     */
    public function getUserById($id)
    {
        return UserFactory::getUserById($id);
    }

    /**
     * @param string $username Unique username of the user
     * @return User
     */
    public function getUserByUsername($username)
    {
        return UserFactory::getUserByUsername($username);
    }
}