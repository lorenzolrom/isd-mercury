<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * DESCRIPTION
 *
 * User: lromero
 * Date: 2/07/2019
 * Time: 12:12 PM
 */


namespace core\models;


use core\classes\ValidationError;

class User
{
    private $id;
    private $username;
    private $firstName;
    private $lastName;
    private $email;
    private $password;
    private $disabled;
    private $authType;

    public function __construct($id, $username, $firstName, $lastName, $email, $password, $disabled, $authType)
    {
        $this->id = $id;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->password = $password;
        $this->disabled = $disabled;
        $this->authType = $authType;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * @return mixed
     */
    public function getAuthType()
    {
        return $this->authType;
    }


    /**
     * @param string $username New username to assign to this user
     * @return int Status code of the UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setUsername($username)
    {
        // Is not the same value as already stored
        if($username == $this->username)
            return ValidationError::VALUE_IS_UNCHANGED;

        // Is not null
        if($username === NULL)
            return ValidationError::VALUE_IS_NULL;

        // Is between 0 and 64 characters
        if(strlen($username) < 1)
            return ValidationError::VALUE_IS_TOO_SHORT;

       if(strlen($username) > 64)
            return ValidationError::VALUE_IS_TOO_LONG;

        // Unique
        else if(UserDatabaseHandler::doesUsernameExist($username))
            return ValidationError::VALUE_IS_ALREADY_TAKEN;

        UserDatabaseHandler::setUsername($this->id, $username);
        $this->username = $username;

        return ValidationError::VALUE_IS_OK;
    }

    /**
     * @param string $firstName New first name for user
     * @return int Status code of the UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setFirstName($firstName)
    {
        // Is not the same value as already stored
        if($firstName == $this->firstName)
            return ValidationError::VALUE_IS_UNCHANGED;

        // Is not null
        if($firstName === NULL)
            return ValidationError::VALUE_IS_NULL;

        // Is between 0 and 32 characters
        if(strlen($firstName) < 1)
            return ValidationError::VALUE_IS_TOO_SHORT;
        if(strlen($firstName) > 32)
            return ValidationError::VALUE_IS_TOO_LONG;

        UserDatabaseHandler::setFirstName($this->id, $firstName);
        $this->firstName = $firstName;

        return ValidationError::VALUE_IS_OK;
    }

    /**
     * @param string $lastName New last name for user
     * @return int Status code of the UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setLastName($lastName)
    {
        // Is not the same value as already stored
        if($lastName == $this->lastName)
            return ValidationError::VALUE_IS_UNCHANGED;

        // Is not null
        if($lastName === NULL)
            return ValidationError::VALUE_IS_NULL;

        // Is between 0 and 32 characters
        if(strlen($lastName) < 1)
            return ValidationError::VALUE_IS_TOO_SHORT;
        if(strlen($lastName) > 32)
            return ValidationError::VALUE_IS_TOO_LONG;

        UserDatabaseHandler::setLastName($this->id, $lastName);
        $this->firstName = $lastName;

        return ValidationError::VALUE_IS_OK;
    }

    /**
     * @param string $email New e-mail address for user
     * @return int Status code of the UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setEmail($email)
    {
        // Is not the same value already stored
        if($email == $this->email)
            return ValidationError::VALUE_IS_UNCHANGED;

        // If not null
        if($email === NULL)
            return ValidationError::VALUE_IS_NULL;

        // If exists, is a valid email address
        if(strlen($email) !== 0 AND !filter_var($email, FILTER_VALIDATE_EMAIL))
            return ValidationError::VALUE_IS_INVALID;

        UserDatabaseHandler::setEmail($this->id, $email);
        $this->email = $email;

        return ValidationError::VALUE_IS_OK;
    }

    /**
     * Creates a double-SHA512 hash from the supplied password
     * @param string $password Raw new password for user
     * @return int Status code of the UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setPassword($password)
    {
        // Is not null
        if($password === NULL)
            return ValidationError::VALUE_IS_NULL;

        // Is greater than 8 characters
        if(strlen($password) < 8)
            return ValidationError::VALUE_IS_TOO_SHORT;

        // Generate double-hashed password
        $hashedPassword = hash('SHA512', hash('SHA512', $password));

        // Is not the same as the current password
        if($hashedPassword == $this->password)
            return ValidationError::VALUE_IS_UNCHANGED;

        UserDatabaseHandler::setPassword($this->id, $hashedPassword);
        $this->password = $password;

        return ValidationError::VALUE_IS_OK;
    }

    /**
     * @param int $disabled New disable value, should be either 0 or 1
     * @return int Status code of UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setDisabled($disabled)
    {
        // New value is not the same as current
        if($this->disabled == $disabled)
            return ValidationError::VALUE_IS_UNCHANGED;

        // Is not null
        if($disabled === NULL)
            return ValidationError::VALUE_IS_NULL;

        // Value is either 1 or 0
        if($disabled != 1 AND $disabled != 0)
            return ValidationError::VALUE_IS_INVALID;

        UserDatabaseHandler::setDisabled($this->id, $disabled);
        $this->disabled = $disabled;

        return ValidationError::VALUE_IS_OK;
    }

    /**
     * @param string $authType New authentication type for user
     * @return int Status code of UPDATE operation
     * @throws \core\classes\DatabaseException
     */
    public function setAuthType($authType)
    {
        // List of acceptable values for this attribute
        $validValues = ['local', 'ldap'];

        // New value is not the same as current
        if($this->authType == $authType)
            return ValidationError::VALUE_IS_UNCHANGED;

        // Is not null
        if($authType === NULL)
            return ValidationError::VALUE_IS_NULL;

        // Is valid
        if(!in_array($authType, $validValues))
            return ValidationError::VALUE_IS_INVALID;

        UserDatabaseHandler::setAuthType($this->id, $authType);
        $this->authType = $authType;

        return ValidationError::VALUE_IS_OK;
    }
}