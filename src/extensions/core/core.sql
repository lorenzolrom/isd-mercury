/*
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Core Database Initialization
 *
 * User: lromero
 * Date: 2/06/2019
 * Time: 5:43 PM
 */

 CREATE TABLE core_User (
   id BIGINT(20) NOT NULL AUTO_INCREMENT,
   username VARCHAR(64) NOT NULL UNIQUE,
   firstName VARCHAR(32) NOT NULL,
   lastName VARCHAR(32) NOT NULL,
   email TEXT NOT NULL,
   password CHAR(128) DEFAULT NULL,
   disabled TINYINT(1) NOT NULL DEFAULT 0,
   authType ENUM('local', 'ldap') NOT NULL DEFAULT 'local',
   PRIMARY KEY (id)
 );

CREATE TABLE core_Role (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL UNIQUE,
  PRIMARY KEY (id)
);

CREATE TABLE core_Token (
  token CHAR(128) NOT NULL,
  user BIGINT(20) NOT NULL,
  issueTime DATETIME NOT NULL,
  expireTime DATETIME NOT NULL,
  isInvalid TINYINT(1) NOT NULL DEFAULT 0,
  ipAddress TEXT NOT NULL,
  PRIMARY KEY (token),
  FOREIGN KEY (user) REFERENCES core_User(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE core_Permission (
  code VARCHAR(64),
  PRIMARY KEY (code)
);

CREATE TABLE core_Attribute (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  extension VARCHAR(32) NOT NULL DEFAULT 'core',
  type VARCHAR(32) NOT NULL,
  code VARCHAR(32) NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (extension, type, code)
);

CREATE TABLE core_Notification (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  user BIGINT(20) DEFAULT NULL,
  title TEXT NOT NULL,
  message TEXT NOT NULL,
  isRead TINYINT(1) NOT NULL DEFAULT 0,
  isDeleted TINYINT(1) NOT NULL DEFAULT 0,
  is_important TINYINT(1) NOT NULL DEFAULT 0,
  sentTime DATETIME NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user) REFERENCES core_User(id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE core_Bulletin (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  author BIGINT(20) DEFAULT NULL,
  startDate DATE NOT NULL,
  endDate DATE NOT NULL,
  title TEXT NOT NULL,
  message TEXT NOT NULL,
  is_active TINYINT(1) NOT NULL DEFAULT 1,
  type ENUM('info', 'alert') NOT NULL DEFAULT 'info',
  PRIMARY KEY (id),
  FOREIGN KEY (author) REFERENCES core_User(id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE core_APIKey (
   apiKey CHAR(128) NOT NULL,
   issue_date DATETIME,
   domain TEXT NOT NULL,
   issueUser BIGINT(20) DEFAULT NULL,
   isValid TINYINT(1) DEFAULT 1,
   PRIMARY KEY(apiKey),
   FOREIGN KEY (issueUser) REFERENCES core_User(id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE core_Route (
  `path` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`path`)
);

/*
Associative Tables
 */

CREATE TABLE core_User_Role (
  user BIGINT(20) NOT NULL,
  role BIGINT(20) NOT NULL,
  PRIMARY KEY(user, role),
  FOREIGN KEY (user) REFERENCES core_User(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (role) REFERENCES core_Role(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE core_Role_Permission (
  role BIGINT(20) NOT NULL,
  permission VARCHAR(64) NOT NULL,
  FOREIGN KEY (role) REFERENCES core_Role(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (permission) REFERENCES core_Permission(code) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE core_Role_Bulletin (
  role BIGINT(20) NOT NULL,
  bulletin BIGINT(20) NOT NULL,
  PRIMARY KEY (role, bulletin),
  FOREIGN KEY (role) REFERENCES core_Role(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (bulletin) REFERENCES core_Bulletin(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE core_APIKey_Route (
  apiKey CHAR(128) NOT NULL,
  route VARCHAR(64) NOT NULL,
  PRIMARY KEY(apiKey, route),
  FOREIGN KEY (apiKey) REFERENCES core_APIKey(apiKey) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (route) REFERENCES core_Route(`path`) ON UPDATE CASCADE ON DELETE CASCADE
);

/*
Insert Default Data
 */

INSERT INTO core_Permission (code) VALUES
  ('core-home-use'),
  ('core-settings-use');

INSERT INTO core_Role (id, name) VALUES
  (1, 'User'),
  (2, 'Power User');

INSERT INTO core_Role_Permission (role, permission) VALUES
  (1, 'core-home-use'),
  (2, 'core-settings-use');

INSERT INTO core_User (id, username, firstName, lastName, password, email) VALUES
  (1, 'admin', 'Built-In', 'Administrator', 'b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86' ,'');

INSERT INTO core_User_Role (user, role) VALUES (1, 1), (1, 2);