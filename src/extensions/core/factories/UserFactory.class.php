<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Generator for User objects
 *
 * User: lromero
 * Date: 2/07/2019
 * Time: 10:19 PM
 */


namespace core\factories;


use core\models\User;
use core\models\UserDatabaseHandler;

class UserFactory
{
    /**
     * @param int $id Numerical I.D. of the user
     * @return User
     */
    public static function getUserById($id)
    {
        $userData = UserDatabaseHandler::fetch($id);

        return self::generateUser($userData);
    }

    /**
     * @param string $username Unique username of the user
     * @return User
     */
    public static function getUserByUsername($username)
    {
        $userData = UserDatabaseHandler::fetchFromUsername($username);
        return self::generateUser($userData);
    }

    /**
     * @param array $userData User attributes from database
     * @return User
     */
    private static function generateUser($userData)
    {
        return new User($userData['id'],
                        $userData['username'],
                        $userData['firstName'],
                        $userData['lastName'],
                        $userData['email'],
                        $userData['password'],
                        $userData['disabled'],
                        $userData['authType']);
    }
}