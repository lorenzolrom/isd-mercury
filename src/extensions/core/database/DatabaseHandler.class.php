<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Interface for a database controller
 *
 * User: lromero
 * Date: 2/07/2019
 * Time: 10:59 AM
 */


namespace core\database;


interface DatabaseHandler
{
    /**
     * @param mixed $pk The primary key of the row to SELECT
     * @return bool | array FALSE if the SELECT operation failed | Array of column values for the selected row
     */
    public static function fetch($pk);

    /**
     * @param mixed $id The primary key of the row to DELETE
     * @return bool Result of DELETE operation
     */
    public static function delete($id);
}