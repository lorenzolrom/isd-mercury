<?php
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * DESCRIPTION
 *
 * User: lromero
 * Date: 2/07/2019
 * Time: 8:25 AM
 */


namespace core\models;

use core\classes\DatabaseConnection;
use core\classes\DatabaseException;
use core\database\DatabaseHandler;

class UserDatabaseHandler implements DatabaseHandler
{
    /**
     * Return array of user details
     * @param int $pk Numerical I.D. of this user
     * @return array|bool Result of SELECT operation
     * @throws DatabaseException
     */
    public static function fetch($pk)
    {
        $handler = new DatabaseConnection();

        $fetch = $handler->prepare("SELECT username, firstName, lastName, email, password, 
                disabled, authType FROM core_User WHERE id = ? LIMIT 1");

        $fetch->bindParam(1, $pk);
        $fetch->execute();

        if($fetch->getRowCount() === 1)
            return $fetch->fetch();

        return FALSE;
    }

    /**
     * @param mixed $id Numerical I.D. of user to be deleted
     * @return bool Was the DELETE operation successful?
     * @throws DatabaseException
     */
    public static function delete($id)
    {
        $handler = new DatabaseConnection();

        $delete = $handler->prepare("DELETE FROM core_User WHERE id = ?");

        $delete->bindParam(1, $id);
        $delete->execute();

        if($delete->getRowCount() === 1)
            return TRUE;

        return FALSE;
    }

    /**
     * Returns user information given only their username
     * @param string $username The unique login username of a user
     * @return bool | array The return value of the fetch() call
     * @throws DatabaseException
     */
    public static function fetchFromUsername($username)
    {
        $handler = new DatabaseConnection();

        $fetch = $handler->prepare("SELECT id FROM core_User WHERE username = ? LIMIT 1");
        $fetch->bindParam(1, $username);
        $fetch->execute();

        if($fetch->getRowCount() === 1)
        {
            return UserDatabaseHandler::fetch($fetch->fetchColumn());
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * @param string $username Username to check
     * @return bool Is the username already in use?
     * @throws DatabaseException
     */
    public static function doesUsernameExist($username)
    {
        $handler = new DatabaseConnection();

        $check = $handler->prepare("SELECT COUNT(1) FROM core_User WHERE username = ? LIMIT 1");
        $check->bindParam(1, $username);
        $check->execute();

        return $check->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param string $username New username
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setUsername($id, $username)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET username = ? WHERE id = ?");
        $update->bindParam(1, $username);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param string $firstName New first name
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setFirstName($id, $firstName)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET firstName = ? WHERE id = ?");
        $update->bindParam(1, $firstName);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param string $lastName New last name
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setLastName($id, $lastName)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET lastName = ? WHERE id = ?");
        $update->bindParam(1, $lastName);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param string $email New email address
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setEmail($id, $email)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET email = ? WHERE id = ?");
        $update->bindParam(1, $email);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param string $hashedPassword Raw value to set the password as in database.  This should be pre-hashed
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setPassword($id, $hashedPassword)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET password = ? WHERE id = ?");
        $update->bindParam(1, $hashedPassword);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param int $disabled New disabled value (0 or 1)
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setDisabled($id, $disabled)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET disabled = ? WHERE id = ?");
        $update->bindParam(1, $disabled);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }

    /**
     * @param int $id ID of user to change
     * @param string $authType New authentication type for user
     * @return bool Has the row been changed
     * @throws DatabaseException
     */
    public static function setAuthType($id, $authType)
    {
        $handler = new DatabaseConnection();

        $update = $handler->prepare("UPDATE core_User SET authType = ? WHERE id = ?");
        $update->bindParam(1, $authType);
        $update->bindParam(2, $id);
        $update->execute();

        return $update->getRowCount() === 1;
    }
}