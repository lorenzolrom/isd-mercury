<?php
namespace core;
/**
 * LLR Technologies & Associated Services
 * Information Systems Development
 *
 * Mercury A.P.I.
 *
 * Configuration
 *
 * User: lromero
 * Date: 2/06/2019
 * Time: 5:43 PM
 */

/* Database */
define('MERCURY_DATA_HOST', 'database.server');
define('MERCURY_DATA_DATABASE', 'database');
define('MERCURY_DATA_USER', 'username');
define('MERCURY_DATA_PASSWORD', 'password');

/* LDAP */
define('MERCURY_LDAP_ENABLED', FALSE);
define('MERCURY_LDAP_DOMAIN_CONTROLLER', 'my.domain.database');
define('MERCURY_LDAP_DOMAIN', 'my.domain.local');
define('MERCURY_LDAP_DOMAIN_DN', 'dc=my,dc=domain,dc=local');
define('MERCURY_LDAP_ADMIN_USERNAME', 'admin');
define('MERCURY_LDAP_ADMIN_PASSWORD', 'password');

/* E-Mail */
define('MERCURY_EMAIL_ENABLED', FALSE);
define('MERCURY_EMAIL_HOST', 'ssl://smpt.eample.com');
define('MERCURY_EMAIL_PORT', '465');
define('MERCURY_EMAIL_AUTH', TRUE);
define('MERCURY_EMAIL_USERNAME', 'username');
define('MERCURY_EMAIL_PASSWORD', 'password');
define('MERCURY_EMAIL_FROM_ADDRESS', 'username@mail.com');
define('MERCURY_EMAIL_FROM_NAME', 'Email Username');